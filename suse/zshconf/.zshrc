# utf-8 in the terminal, will break stuff if your term isn't utf aware
LANG=en_US.UTF-8 
LC_ALL=$LANG
LC_COLLATE=C

# Export
export EDITOR="mcedit"
export VISUAL="mcedit"
export BROWSER="gooogle-chrome-stable"
setopt ZLE

# Basic Configuration
autoload zkbd
source ~/.zkbd/$TERM-:0.0 # may be different - check where zkbd saved the configuration:

[[ -n ${key[Backspace]} ]] && bindkey "${key[Backspace]}" backward-delete-char
[[ -n ${key[Insert]} ]] && bindkey "${key[Insert]}" overwrite-mode
[[ -n ${key[End]} ]] && bindkey "${key[End]}" beginning-of-line
[[ -n ${key[PageUp]} ]] && bindkey "${key[PageUp]}" up-line-or-history
[[ -n ${key[Delete]} ]] && bindkey "${key[Delete]}" delete-char
[[ -n ${key[Home]} ]] && bindkey "${key[Home]}" end-of-line
[[ -n ${key[PageDown]} ]] && bindkey "${key[PageDown]}" down-line-or-history

autoload -U compinit 
autoload -U promptinit
autoload -U colors
autoload -U zrecompile
autoload edit-command-line
autoload -U zmv
compinit
promptinit
colors

REPORTTIME=60       # Report time statistics for progs that take more than a minute to run
WATCH=notme         # Report any login/logout of other users
WATCHFMT='%n %a %l from %m at %T.'

# Prompt
PROMPT='%{$fg_bold[red]%}%n:%{$reset_color%}%{$fg_bold[green]%} %{$reset_color%}% %{$fg_bold[blue]%}%1~ %{$fg_bold[magneta]%}%(?.:%).:() %f%{$reset_color%}:%{$reset_color%} '
RPROMPT='%F{green}%T%f'

# Prompt with full path
# PROMPT='%{$fg_bold[red]%}%n:%{$reset_color%}%{$fg_bold[green]%} %{$reset_color%}% %{$fg_bold[blue]$(pwd)%${#PWD}G%} %{$fg_bold[magneta]%}%(?.:%).:() %f%{$reset_color%}:%{$reset_color%} '

# Set Option
setopt NO_FLOW_CONTROL
setopt                          \
        auto_cd                 \
        auto_pushd              \
        append_history        \
        no_flow_control		\
        chase_links             \
        noclobber               \
        complete_aliases        \
        extended_glob           \
        hist_ignore_all_dups    \
        hist_ignore_space       \
        ignore_eof              \
        share_history           \
        no_flow_control         \
        list_types              \
        mark_dirs               \
        path_dirs               \
        prompt_percent          \
        prompt_subst            \
        rm_star_wait		\
        zle

# Push a command onto a stack allowing you to run another command first
bindkey '^J' push-line




# History Config
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history
eval `dircolors -b`

# Zstyle
zstyle ':completion:*' menu select
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache
zstyle ':completion:*:match:*' original only
zstyle ':completion:*:approximate:*' max-errors 1 numeric
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' completions 1
zstyle ':completion:*' file-sort name
zstyle ':completion:*' glob 1
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' max-errors 2
zstyle ':completion:*' original true
zstyle ':completion:*' prompt 'correction: %e '
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' substitute 1

# ALIAS
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias vmc='vuurmuur_conf'
alias pinstall='sudo zypper install'
alias plinstall='sudo zypper in'
alias premove='sudo zypper remove'
alias psearch='sudo zypper search'
alias arte='php artisan'
alias taill='tail --lines=100'
alias tailf='tail -fn=100'
alias wgetn='wget --no-check-certificate'
alias fcache='fc-cache -vfs'
alias ..='cd ..'
alias df='df -h'
alias ls='ls --group-directories-first --color=auto'
alias LS='ls -al --group-directories-first --color=auto'
alias p='pwd'
alias grep='grep --color'
alias rgrep='grep -R --color'
alias hset='hsetroot -fill'
alias kfc='killall conky'
alias off='sudo halt'
alias reboot='sudo reboot'
alias mount='sudo mount'
alias umount='sudo umount'
alias usb='mount /dev/sdb /mnt/usb'
alias fm='ranger'
alias v='mcedit'
alias vs='sudo mcedit'
alias gcommit='git commit -m'
alias gpush='git push origin'
alias gadd='git add . -A'
alias pid='pgrep'
alias edsh='mcedit ~/.zshrc'
alias pman='python manage.py'
alias sstatus='systemctl status'
alias sstart='systemctl start'
alias srestart='systemctl restart'
alias sstop='systemctl stop'

# Functions
# Insert "sudo " at the beginning of the line
#function prepend-sudo {
#  if [[ $BUFFER != "sudo "* ]]; then
#    BUFFER="sudo $BUFFER"; CURSOR+=5
#  fi
#}
#zle -N prepend-sudo
#[[ -n "$keyinfo[Control]" ]] && \
#  bindkey "$keyinfo[Control]s" prepend-sudo
 insert_sudo () { zle beginning-of-line; zle -U "sudo " }
zle -N insert-sudo insert_sudo
bindkey "^[s" insert-sudo

# Compression #
ex() {
  if [ -f $1 ] ; then
      case $1 in
          *.tar.bz2)   tar xvjf $1    ;;
          *.tar.gz)    tar xvzf $1    ;;
          *.bz2)       bunzip2 $1     ;;
          *.rar)       rar x $1       ;;
          *.gz)        gunzip $1      ;;
          *.tar)       tar xvf $1     ;;
          *.tbz2)      tar xvjf $1    ;;
          *.tgz)       tar xvzf $1    ;;
          *.tar.xz)    tar Jxf $1     ;; 
          *.zip)       unzip $1       ;;
          *.Z)         uncompress $1  ;;
          *.7z)        7z x $1        ;;
          *)           echo "don't know how to extract '$1'..." ;;
      esac
  else
      echo "'$1' is not a valid file!"
  fi
}
